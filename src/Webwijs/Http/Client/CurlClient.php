<?php

namespace Webwijs\Http\Client;

use CurlHandle;

class CurlClient implements ClientInterface
{
    private array $options = [
        CURLOPT_RETURNTRANSFER => 1,
    ];
    
    public function get( string $url, RequestInterface $request ): ResponseInterface
    {
        return $this->execute( $this->createCurlHandle( $url, $request ) );
    }
    
    public function post( string $url, RequestInterface $request ): ResponseInterface
    {
        $ch = $this->createCurlHandle( $url, $request );
        
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $request->getBody() );
        
        return $this->execute( $ch );
    }
    
    public function put( string $url, RequestInterface $request ): ResponseInterface
    {
        $ch = $this->createCurlHandle( $url, $request );
        
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'PUT' );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $request->getBody() );
        
        return $this->execute( $ch );
    }
    
    public function delete( string $url, RequestInterface $request ): ResponseInterface
    {
        $ch = $this->createCurlHandle( $url, $request );
        
        curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'DELETE' );
        
        return $this->execute( $ch );
    }
    
    private function createCurlHandle( string $url, RequestInterface $request ): CurlHandle
    {
        $ch = curl_init();
        
        curl_setopt_array( $ch, $this->options );
        
        curl_setopt( $ch, CURLOPT_URL, $url );
        
        curl_setopt( $ch, CURLOPT_HEADER, 1 );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $this->implodeHeaders( $request->getHeaders() ) );
        
        return $ch;
    }
    
    private function execute( CurlHandle $ch ): ResponseInterface
    {
        $response   = (string) curl_exec( $ch );
        $statusCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        curl_close( $ch );
        
        [ $header, $body ] = explode( "\r\n\r\n", $response, 2 );
        
        return new Response( $statusCode, $body, $this->explodeHeader( $header ) );
    }
    
    private function implodeHeaders( array $headers ): array
    {
        $implodedHeaders = [];
        foreach ( $headers as $key => $val ) {
            $implodedHeaders[] = $key . ': ' . $val;
        }
        
        return $implodedHeaders;
    }
    
    private function explodeHeader( string $header ): array
    {
        $headers = [];
        
        foreach ( explode( "\r\n", $header ) as $i => $line ) {
            // skip the HTTP status code on line 1
            if ( $i === 0 ) {
                continue;
            }
            
            [ $key, $value ] = explode( ': ', $line );
            $headers[ $key ] = $value;
        }
        
        return $headers;
    }
}