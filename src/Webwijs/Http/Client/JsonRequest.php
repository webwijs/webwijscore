<?php

namespace Webwijs\Http\Client;

class JsonRequest extends AbstractRequest
{
    public function __construct( array $data = [], array $headers = [] )
    {
        $defaultHeaders = [ 'Content-Type' => 'application/json' ];
        
        parent::__construct( json_encode( $data ), array_merge( $defaultHeaders, $headers ) );
    }
}