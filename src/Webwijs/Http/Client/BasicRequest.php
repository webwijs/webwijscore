<?php

namespace Webwijs\Http\Client;

use http\Exception\InvalidArgumentException;

class BasicRequest extends AbstractRequest
{
    /**
     * @param string|array $data
     * @param array $headers
     */
    public function __construct( string|array $data, array $headers = [] ) // @TODO ADD MIXED TYPE FOR $data
    {
        $defaultHeaders = [];
        
        if ( is_array( $data ) ) {
            $defaultHeaders['Content-Type'] = 'multipart/form-data';
        } elseif ( is_string( $data ) ) {
            $defaultHeaders['Content-Type'] = 'application/x-www-form-urlencoded';
        } else {
            throw new InvalidArgumentException(
                'The first argument of ' . self::class . '::construct() should be an array or a string. '
            );
        }
        
        $headers = array_merge( $defaultHeaders, $headers );
        
        parent::__construct( $data, $headers );
    }
}