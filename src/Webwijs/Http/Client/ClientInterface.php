<?php

namespace Webwijs\Http\Client;

interface ClientInterface
{
    public function get( string $url, RequestInterface $request ): ResponseInterface;
    
    public function post(  string $url, RequestInterface $request ): ResponseInterface;
    
    public function put( string $url, RequestInterface $request ): ResponseInterface;
    
    public function delete( string $url, RequestInterface $request ): ResponseInterface;
}