<?php

namespace Webwijs\Http\Client;

use DOMDocument;

class XmlRequest extends AbstractRequest
{
    public function __construct( DOMDocument $body, array $headers = [] )
    {
        $defaultHeaders = [ 'Content-Type' => 'application/xml' ];
    
        parent::__construct( $body->saveXML(), array_merge( $defaultHeaders, $headers ) );
    }
}