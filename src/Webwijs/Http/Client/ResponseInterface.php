<?php

namespace Webwijs\Http\Client;

interface ResponseInterface
{
    public function getStatusCode(): int;
    
    public function getBody(): string;
    
    public function getHeaders(): array;
}