<?php

namespace Webwijs\Http\Client;

abstract class AbstractRequest implements RequestInterface
{
    private array $headers;
    
    private string|array $body;
    
    /**
     * @param array|string $body
     * @param array $headers
     */
    public function __construct( string|array $body, array $headers = [] )
    {
        $this->body = $body;
        $this->headers = $headers;
    }
    
    public function getHeaders(): array
    {
        return $this->headers;
    }
    
    public function getBody(): string|array
    {
        return $this->body;
    }
}