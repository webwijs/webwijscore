<?php

namespace Webwijs\Http\Client;

interface RequestInterface
{
    public function getHeaders(): array;
    
    public function getBody(): string|array;
}