<?php

namespace Webwijs\Dom;

/**
 * The HtmlElementBuilderInterface provides a fluent interface for the creation of {@link HtmlElement} instances.
 *
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.0.0
 * @since 1.1.0
 */
interface HtmlElementBuilderInterface
{
    /**
     * Append an attribute to the builder that will be added to the {@link HtmlElement} instance.
     *
     * @param string $name the attribute name.
     * @param mixed $value the attribute value.
     * @return HtmlElementBuilderInterface the builder which allows further creation of the element. 
     */
    public function attribute($name, $value);
    
    /**
     * Append a piece of arbitrary data to the builder that will be added to the {@link HtmlElement} instance.
     * 
     * @param string $name the name by which the data is identified.
     * @param mixed $value the value to associate with the specified name.
     * @return HtmlElementBuilderInterface the builder which allows further creation of the element. 
     */ 
    public function data($name, $value);
   
    /**
     * Set the HTML tag that the element will represent.
     * 
     * @param string $tag the HTML tag.
     * @return HtmlElementBuilderInterface the builder which allows further creation of the element. 
     */ 
    public function tag($tag);
    
    /**
     * Append a child element to the builder that will be added to the {@link HtmlElement} instance.
     * 
     * @param ElementInterface|callable $child the child element or a callable that creates the element to append.
     * @return HtmlElementBuilderInterface the builder which allows further creation of the element.
     */
    public function child($child);
    
    /**
     * Set the inner text that the builder will set for the {@link HtmlElement} instance.
     *
     * @param string $text the inner text.
     * @return HtmlElementBuilderInterface the builder which allows further creation of the element.
     */
    public function text($text);
    
    /**
     * Build a new {@link HtmlElement} instance for arguments that were provided to the buider.
     *
     * @return ElementInterface a newly instantiated {@link HtmlElement} instance.
     */
    public function build();
}
