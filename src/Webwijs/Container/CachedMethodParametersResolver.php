<?php

namespace Webwijs\Container;

use Doctrine\Common\Cache\Cache;
use ReflectionMethod;

class CachedMethodParametersResolver extends MethodParametersResolver
{
    private Cache $cache;
    
    /**
     * CachedMethodParametersResolver constructor.
     *
     * @param ParameterResolverInterface $parameterResolver
     * @param Cache $cache
     */
    public function __construct( ParameterResolverInterface $parameterResolver, Cache $cache )
    {
        parent::__construct( $parameterResolver );
        
        $this->cache = $cache;
    }
    
    public function resolve( ContainerInterface $container, ReflectionMethod $reflectionMethod ): array
    {
        $cacheKey = $reflectionMethod->getDeclaringClass()->getName() . '::' . $reflectionMethod->getName();
        
        if ( $this->cache->contains( $cacheKey ) ) {
            return unserialize( $this->cache->fetch( $cacheKey ), true );
        }
        
        $args = parent::resolve( $container, $reflectionMethod );
        $this->cache->save( $cacheKey, serialize( $args ) );
        
        return $args;
    }
}