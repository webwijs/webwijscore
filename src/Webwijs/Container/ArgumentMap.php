<?php


namespace Webwijs\Container;


use Webwijs\Container\Exception\UnspecifiedArgumentException;

class ArgumentMap implements ArgumentMapInterface
{
    private array $methodArguments;
    
    /**
     * ArgumentMap constructor.
     *
     * @param array $methodArguments
     */
    public function __construct( array $methodArguments = [] )
    {
        $this->methodArguments = $methodArguments;
    }
    
    
    /**
     * {@inheritDoc}
     */
    public function set( string $className, string $methodName, string $argumentName, $argumentValue ): self
    {
        if ( ! isset( $this->methodArguments[ $className ] ) ) {
            $this->methodArguments[ $className ] = [];
        }
        
        if ( ! isset( $this->methodArguments[ $className ][ $methodName ] ) ) {
            $this->methodArguments[ $className ][ $methodName ] = [];
        }
        
        $this->methodArguments[ $className ][ $methodName ][ $argumentName ] = $argumentValue;
        
        return $this;
    }
    
    public function setArguments( string $className, string $methodName, array $arguments ): self
    {
        foreach ($arguments as $name => $value) {
            $this->set($className, $methodName, $name, $value);
        }
        
        return $this;
    }
    
    /**
     * {@inheritDoc}
     */
    public function get( string $className, string $methodName, string $argumentName )
    {
        // $arg = @$this->methodArguments[ $className ][ $methodName ][ $argumentName ];
        // Het probleem met deze aanpak is dat de @ foutmeldingen en notices onderdrukt en null teruggeeft.
        // Het ingestelde argument mag de waarde NULL hebben.
        // Daarom is het niet te gebruiken om te bepalen of de waarde is ingesteld. Daarom maar een extra dure if-check.
        
        $args = @$this->methodArguments[ $className ][ $methodName ];
        
        if ( ! is_array( $args ) ) {
            throw new UnspecifiedArgumentException( sprintf(
                'There are no arguments specified for "%s::%s"',
                $className,
                $methodName,
            ) );
        }
        
        if ( ! isset( $args[ $argumentName ] ) ) {
            throw new UnspecifiedArgumentException( sprintf(
                'There is no argument named "%s" specified for "%s::%s"',
                $argumentName,
                $className,
                $methodName
            ) );
        }
        
        return $this->methodArguments[ $className ][ $methodName ][ $argumentName ];
    }
    
    
}