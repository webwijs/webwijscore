<?php


namespace Webwijs\Container;


use Webwijs\Container\Exception\UnknownResolverException;

class ResolverMap implements ResolverMapInterface
{
    private array $resolvers = [];
    
    public function __construct( array $resolvers = [] )
    {
        foreach ( $resolvers as $className => $resolver ) {
            $this->set($className, $resolver);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public function set( string $className, ResolverInterface $resolver ): self
    {
        $this->resolvers[ $className ] = $resolver;
        
        return $this;
    }
    
    /**
     * {@inheritDoc}
     */
    public function get( string $className ): ResolverInterface
    {
        if ( ! isset( $this->resolvers[ $className ] ) ) {
            throw new UnknownResolverException( 'No resolver was defined for "' . $className . '"' );
        }
        
        return $this->resolvers[ $className ];
    }
}