<?php


namespace Webwijs\Container;


use Webwijs\Container\Exception\UnspecifiedArgumentException;

interface ArgumentMapInterface
{
    /**
     * @param string $className
     * @param string $methodName
     * @param string $argumentName
     * @param $argumentValue
     *
     * @return self
     */
    public function set( string $className, string $methodName, string $argumentName, $argumentValue ): self;
    
    public function setArguments( string $className, string $methodName, array $arguments ): self;
    
    /**
     * @param string $className
     * @param string $methodName
     * @param string $argumentName
     *
     * @return mixed
     * @throws UnspecifiedArgumentException
     */
    public function get( string $className, string $methodName, string $argumentName);
}