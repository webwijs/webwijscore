<?php


namespace Webwijs\Container;


use ReflectionParameter;
use Webwijs\Container\Exception\UnresolvableParameterException;

class ParameterResolver implements ParameterResolverInterface
{
    /**
     * @var ParameterResolverInterface[]
     */
    private array $resolvers;
    
    /**
     * ArgumentResolver constructor.
     *
     * @param ParameterResolverInterface[] $resolvers
     */
    public function __construct( array $resolvers )
    {
        $this->resolvers = [];
        foreach ( $resolvers as $resolver ) {
            $this->addResolver( $resolver );
        }
    }
    
    
    public function resolve( ContainerInterface $container, ReflectionParameter $reflectionParameter )
    {
        
        foreach ( $this->resolvers as $resolver ) {
            try {
                return $resolver->resolve( $container, $reflectionParameter );
            } catch ( UnresolvableParameterException $e ) {
                continue;
            }
        }
    
        throw new UnresolvableParameterException( sprintf( 'Unable to resolve parameter "%s" for "%s::%s"',
            $reflectionParameter->getName(),
            $reflectionParameter->getDeclaringClass(),
            $reflectionParameter->getDeclaringFunction()
        ) );
    }
    
    private function addResolver( ParameterResolverInterface $resolver )
    {
        $this->resolvers[] = $resolver;
    }
}