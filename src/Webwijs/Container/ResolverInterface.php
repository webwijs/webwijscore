<?php

namespace Webwijs\Container;

use ReflectionException;

interface ResolverInterface
{
	/**
	 * @param ContainerInterface $container
	 *
	 * @return object
	 *
	 * @throws ReflectionException
	 */
	public function resolve( ContainerInterface $container ): object;
}