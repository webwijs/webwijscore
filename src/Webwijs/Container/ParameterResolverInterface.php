<?php


namespace Webwijs\Container;

use ReflectionMethod;
use ReflectionParameter;
use Webwijs\Container\Exception\UnresolvableParameterException;

interface ParameterResolverInterface
{
    /**
     * @param ContainerInterface $container
     * @param ReflectionParameter $reflectionParameter
     *
     * @return mixed
     * @throws UnresolvableParameterException
     */
    public function resolve( ContainerInterface $container, ReflectionParameter $reflectionParameter );
}