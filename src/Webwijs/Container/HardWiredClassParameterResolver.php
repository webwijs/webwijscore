<?php


namespace Webwijs\Container;

use ReflectionException;
use ReflectionParameter;
use Webwijs\Container\Exception\AutoWiringException;
use Webwijs\Container\Exception\InvalidServiceException;
use Webwijs\Container\Exception\UnknownServiceException;
use Webwijs\Container\Exception\UnresolvableParameterException;

class HardWiredClassParameterResolver extends HardWiredParameterResolver
{
    public function resolve( ContainerInterface $container, ReflectionParameter $reflectionParameter )
    {
        try {
            $arg = parent::resolve( $container, $reflectionParameter );
            if ( @substr( $arg, 0, 1 ) === '@' ) {
                return $container->get( ltrim( $arg, '@' ) );
            }
        } catch ( ReflectionException $e ) {
        } catch ( AutoWiringException $e ) {
        } catch ( InvalidServiceException $e ) {
        } catch ( UnknownServiceException $e ) {
        }
    
        throw new UnresolvableParameterException( sprintf( 'Unable to hard-wire parameter "%s" for "%s::%s"',
            $reflectionParameter->getName(),
            $reflectionParameter->getDeclaringClass(),
            $reflectionParameter->getDeclaringFunction()
        ) );
    }
}