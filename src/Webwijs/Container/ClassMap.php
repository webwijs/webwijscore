<?php


namespace Webwijs\Container;


class ClassMap implements ClassMapInterface
{
    /**
     * @var array
     */
    private array $map;
    
    public function __construct( array $map = [] )
    {
        $this->map = $map;
    }
    
    public function set( string $className, string $mappedClassName ): self
    {
        $this->map[ $className ] = $mappedClassName;
        
        return $this;
    }
    
    public function get( string $className ): ?string
    {
        return isset( $this->map[ $className ] )
            ? $this->map[ $className ]
            : null;
    }
}