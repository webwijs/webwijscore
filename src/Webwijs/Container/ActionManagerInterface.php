<?php


namespace Webwijs\Container;


interface ActionManagerInterface
{
    public function addAction(
        string $action,
        string $className,
        string $methodName,
        int $priority = 10,
        int $acceptedArgs = 1
    ): self;
    
    public function addFilter(
        string $action,
        string $className,
        string $methodName,
        int $priority = 10,
        int $acceptedArgs = 1
    ): self;
}