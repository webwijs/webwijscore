<?php


namespace Webwijs\Container;


use Webwijs\Container\Exception\UnknownResolverException;

interface ResolverMapInterface
{
    /**
     * @param string $className
     * @param ResolverInterface $resolver
     *
     * @return $this
     */
    public function set( string $className, ResolverInterface $resolver ): self;
    
    /**
     * @param string $className
     *
     * @return ResolverInterface
     * @throws UnknownResolverException
     */
    public function get( string $className ): ResolverInterface;
}