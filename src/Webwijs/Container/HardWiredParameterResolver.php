<?php


namespace Webwijs\Container;


use ReflectionParameter;
use Webwijs\Container\Exception\UnresolvableParameterException;
use Webwijs\Container\Exception\UnspecifiedArgumentException;

class HardWiredParameterResolver implements ParameterResolverInterface
{
    public function resolve( ContainerInterface $container, ReflectionParameter $reflectionParameter )
    {
        try {
            return $container->getMethodArguments()->get(
                $reflectionParameter->getDeclaringClass(),
                $reflectionParameter->getDeclaringFunction(),
                $reflectionParameter->getName()
            )
                ;
        } catch ( UnspecifiedArgumentException $e ) {
            throw new UnresolvableParameterException( sprintf( 'Unable to hard-wire parameter "%s" for "%s::%s"',
                $reflectionParameter->getName(),
                $reflectionParameter->getDeclaringClass(),
                $reflectionParameter->getDeclaringFunction()
            ) );
        }
    }
}