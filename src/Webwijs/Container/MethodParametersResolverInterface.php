<?php


namespace Webwijs\Container;


use ReflectionMethod;

interface MethodParametersResolverInterface
{
    public function resolve( ContainerInterface $container, ReflectionMethod $reflectionMethod ): array;
}