<?php


namespace Webwijs\Container;

interface ClassMapInterface
{
    public function set( string $className, string $mappedClassName ): self;
    
    public function get( string $className ): ?string;
}