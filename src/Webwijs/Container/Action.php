<?php


namespace Webwijs\Container;


class Action
{
    private ContainerInterface $container;
    
    private string $className;
    
    private string $methodName;
    
    /**
     * Action constructor.
     *
     * @param string $className
     * @param string $methodName
     */
    public function __construct( ContainerInterface $container, string $className, string $methodName )
    {
        $this->container  = $container;
        $this->className  = $className;
        $this->methodName = $methodName;
    }
    
    public function __invoke( ...$args )
    {
        $serviceInstance = $this->container->get( $this->className );
        return call_user_func_array([$serviceInstance, $this->methodName], $args);
//        $this->container->call( $this->className, $this->methodName, $args );
    }
}