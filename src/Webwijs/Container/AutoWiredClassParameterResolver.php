<?php


namespace Webwijs\Container;


use Exception;
use ReflectionParameter;
use Webwijs\Container\Exception\UnresolvableParameterException;

class AutoWiredClassParameterResolver implements ParameterResolverInterface
{
    public function resolve( ContainerInterface $container, ReflectionParameter $reflectionParameter )
    {
        try {
//            // ReflectionParameter::getClass(); is deprecated since PHP8.0
//            // https://www.php.net/manual/en/reflectionparameter.getclass.php
//            $class = $reflectionParameter->getClass();
//
//            if ( $class !== null ) {
//                return $container->get( $class->getName() );
//            }
            
            // use ReflectionParameter::getType() to get the ReflectionType of the parameter,
            // then interrogate that object to determine the parameter type.
            $type = $reflectionParameter->getType();
            if ( $type !== null ) {
                return $container->get( $type->getName() );
            }
            
        } catch ( Exception $e ) {
        }
        
        throw new UnresolvableParameterException( sprintf( 'Unable to auto-wire parameter "%s" for "%s::%s"',
            $reflectionParameter->getName(),
            $reflectionParameter->getDeclaringClass(),
            $reflectionParameter->getDeclaringFunction()
        ) );
    }
}