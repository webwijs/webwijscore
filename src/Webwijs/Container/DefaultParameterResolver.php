<?php


namespace Webwijs\Container;

use ReflectionException;
use ReflectionParameter;
use Webwijs\Container\Exception\UnresolvableParameterException;

class DefaultParameterResolver implements ParameterResolverInterface
{
    public function resolve( ContainerInterface $container, ReflectionParameter $reflectionParameter )
    {
        if ( $reflectionParameter->isDefaultValueAvailable() ) {
            try {
                return $reflectionParameter->getDefaultValue();
            } catch ( ReflectionException $e ) {
            }
        }
    
        throw new UnresolvableParameterException( sprintf( 'Unable to hard-wire parameter "%s" for "%s::%s"',
            $reflectionParameter->getName(),
            $reflectionParameter->getDeclaringClass(),
            $reflectionParameter->getDeclaringFunction()
        ) );
    }
}