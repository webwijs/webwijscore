<?php


namespace Webwijs\Container;


use ReflectionMethod;
use Webwijs\Container\Exception\UnresolvableParameterException;

class MethodParametersResolver implements MethodParametersResolverInterface
{
    /**
     * @var ParameterResolverInterface
     */
    private ParameterResolverInterface $parameterResolver;
    
    /**
     * MethodArgumentsResolver constructor.
     *
     * @param ParameterResolverInterface $parameterResolver
     */
    public function __construct( ParameterResolverInterface $parameterResolver )
    {
        $this->parameterResolver = $parameterResolver;
    }
    
    public function resolve( ContainerInterface $container, ReflectionMethod $reflectionMethod ): array
    {
        $args = [];
        
        foreach ( $reflectionMethod->getParameters() as $reflectionParameter ) {
            $args[ $reflectionParameter->getName() ] = null;
            try {
                $args[ $reflectionParameter->getName() ] = $this->parameterResolver->resolve( $container, $reflectionParameter );
                continue;
            } catch ( UnresolvableParameterException $e ) {
            }
        }
        
        return $args;
    }
}