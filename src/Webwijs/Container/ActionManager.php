<?php


namespace Webwijs\Container;


class ActionManager implements ActionManagerInterface
{
    private ContainerInterface $container;
    
    public function __construct( ContainerInterface $container )
    {
        $this->container = $container;
    }
    
    public function addAction(
        string $action,
        string $className,
        string $methodName,
        int $priority = 10,
        int $acceptedArgs = 1
    ): self
    {
        add_action(
            $action,
            new Action( $this->container, $className, $methodName ),
            $priority,
            $acceptedArgs
        );
        
        return $this;
    }
    
    public function addFilter(
        string $action,
        string $className,
        string $methodName,
        int $priority = 10,
        int $acceptedArgs = 1
    ): self
    {
        add_filter(
            $action,
            new Action( $this->container, $className, $methodName ),
            $priority,
            $acceptedArgs
        );
    
        return $this;
    }
}