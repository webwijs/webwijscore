<?php

namespace Webwijs\Container;

use Exception;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionParameter;
use Webwijs\Container\Exception\AutoWiringException;
use Webwijs\Container\Exception\InvalidConfigException;
use Webwijs\Container\Exception\InvalidServiceException;
use Webwijs\Container\Exception\UnknownResolverException;
use Webwijs\Container\Exception\UnknownServiceException;
use Webwijs\Container\Exception\UnspecifiedArgumentException;

class Container implements ContainerInterface
{
    /**
     * @var ResolverMapInterface
     */
    private ResolverMapInterface $resolvers;
    
    /**
     * @var mixed[]
     */
    private array $config;
    
    /**
     * @var MethodParametersResolverInterface
     */
    private MethodParametersResolverInterface $parametersResolver;
    
    /**
     * @var ClassMapInterface
     */
    private ClassMapInterface $classMap;
    
    /**
     * @var ArgumentMapInterface
     */
    private ArgumentMapInterface $methodArguments;
    
    
    /**
     * @var mixed[]
     */
    private array $instances;
    
    /**
     * Container constructor.
     *
     * @param MethodParametersResolverInterface $parametersResolver
     * @param array $config
     * @param ResolverMapInterface|null $resolvers
     * @param ClassMapInterface|null $classMap
     * @param ArgumentMapInterface|null $methodArguments
     */
    public function __construct(
        MethodParametersResolverInterface $parametersResolver,
        array $config = [],
        ResolverMapInterface $resolvers = null,
        ClassMapInterface $classMap = null,
        ArgumentMapInterface $methodArguments = null
    ) {
        $this->parametersResolver                     = $parametersResolver;
        $this->config                                 = $config;
        $this->resolvers                              = $resolvers !== null ? $resolvers : new ResolverMap();
        $this->classMap                               = $classMap !== null ? $classMap : new ClassMap();
        $this->methodArguments                        = $methodArguments !== null ? $methodArguments : new ArgumentMap();
        $this->instances[ ContainerInterface::class ] = $this;
        $this->instances[ Container::class ]          = $this;
    }
    
    /**
     * {@inheritDoc}
     */
    public function addResolver( string $className, ResolverInterface $resolver ): ContainerInterface
    {
        $this->resolvers->set( $className, $resolver );
        
        return $this;
    }
    
    /**
     * {@inheritDoc}
     */
    public function get( string $className )
    {
        $className = ( $mappedClass = $this->getClassMap()->get( $className ) )
            ? $mappedClass : $className;
        
        if ( empty( $this->instances[ $className ] ) ) {
            $this->resolve( $className );
        }
        
        if ( ! is_a( $this->instances[ $className ], $className ) ) {
            throw new InvalidServiceException( sprintf(
                'Invalid object! Requested an instance of "%s" but received a "%s.',
                $className,
                get_class( $this->instances[ $className ] )
            ) );
        }
        
        return $this->instances[ $className ];
    }
    
    /**
     * @param string $className
     *
     * @throws UnknownServiceException
     * @throws ReflectionException
     * @throws AutoWiringException
     */
    private function resolve( string $className )
    {
        // if a manual resolver is present in the DI config, this has the highest priority
        try {
            $this->getResolvers()->get( $className )->resolve( $this );
            
            return;
        } catch ( UnknownResolverException $e ) {
            // try next resolving method
        }
    
        // if auto-wiring is enabled, try to auto-wire it
        try {
            if ( $this->getConfig( 'auto-wiring' ) === true ) {
                $this->instances[ $className ] = $this->autoWire( $className );
                
                return;
            }
        } catch ( Exception $e ) {
            // try next resolving method
        }
        
        throw new UnknownServiceException( sprintf(
            'The container was unable to resolve "%s"',
            $className
        ) );
    }
    
    /**
     * @param string $className
     *
     * @return mixed
     * @throws AutoWiringException|ReflectionException
     */
    private function autoWire( string $className )
    {
        try {
            $reflectionClass = new ReflectionClass( $className );
        } catch ( ReflectionException $e ) {
            throw new AutoWiringException( 'Class with name ' . $className . ' does not exist' );
        }
        
        try {
            return $reflectionClass->newInstanceArgs(
                $this->parametersResolver->resolve( $this, $reflectionClass->getMethod( '__construct' ) )
            );
        } catch ( ReflectionException $e ) {
            // when the constructor doesn't accept arguments, call the constructor empty
            return $reflectionClass->newInstance();
        }
    }
    
    public function setConfig( string $key, $value ): ContainerInterface
    {
        $this->config[ $key ] = $value;
        
        return $this;
    }
    
    public function getConfig( string $key )
    {
        if ( ! isset( $this->config[ $key ] ) ) {
            throw new InvalidConfigException( sprintf( 'The config option with key %s does not exist', $key ) );
        }
        
        return $this->config[ $key ];
    }
    
    /**
     * @param string $className
     * @param string $methodName
     * @param array $args
     *
     * @return mixed|void
     * @throws AutoWiringException
     * @throws InvalidServiceException
     * @throws UnknownServiceException
     */
    public function call( string $className, string $methodName, array $args = [] )
    {
        try {
            $reflectionClass = new ReflectionClass( $className );
            
            if ( count( $args ) < 1 ) {
                $args = $this->parametersResolver->resolve( $this, $reflectionClass->getMethod( $methodName ) );
            }
            
            $instance = $this->get( $className );
            
            return call_user_func_array( [ $instance, $methodName ], $args );
            
        } catch ( ReflectionException $e ) {
            throw new UnknownServiceException( sprintf(
                'The container was unable to call method "%s" of class "%s',
                $methodName,
                $className
            ) );
        }
    }
    
    public function getClassMap(): ClassMapInterface
    {
        return $this->classMap;
    }
    
    public function getMethodArguments(): ArgumentMapInterface
    {
        return $this->methodArguments;
    }
    
    public function getResolvers(): ResolverMapInterface
    {
        return $this->resolvers;
    }
}