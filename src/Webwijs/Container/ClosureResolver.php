<?php

namespace Webwijs\Container;

use Closure;
use ReflectionFunction;

class ClosureResolver implements ResolverInterface
{
    /**
     * @var Closure
     */
    private Closure $closure;
    
    /**
     * ClosureServiceInitializer constructor.
     *
     * @param $closure
     */
    public function __construct( Closure $closure )
    {
        $this->closure = $closure;
    }
    
    /**
     * {@inheritDoc}
     */
    public function resolve( ContainerInterface $container ): object
    {
        $closure = $this->closure;
        
        $closureReflection    = new ReflectionFunction( $closure );
        $reflectionParameters = $closureReflection->getParameters();
        
        if ( ! isset( $reflectionParameters[0] ) ) {
            return $closure();
        }
        
        $firstArgumentType = $reflectionParameters[0]->getType();
        
        if ( $firstArgumentType !== null && $firstArgumentType->getName() === ContainerInterface::class ) {
            return $closure( $container );
        }
        
        return $closure();
    }
}