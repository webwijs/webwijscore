<?php

namespace Webwijs\Container;

use ReflectionException;
use Webwijs\Container\Exception\AutoWiringException;
use Webwijs\Container\Exception\InvalidConfigException;
use Webwijs\Container\Exception\UnknownServiceException;
use Webwijs\Container\Exception\InvalidServiceException;

interface ContainerInterface
{
    /**
     * @param string $className
     *
     * @return mixed
     * @throws UnknownServiceException
     * @throws InvalidServiceException
     * @throws ReflectionException
     * @throws AutoWiringException
     */
    public function get( string $className );
    
    /**
     * @param string $className
     * @param ResolverInterface $resolver
     *
     * @return ContainerInterface self
     * @throws ReflectionException
     */
    public function addResolver( string $className, ResolverInterface $resolver ): ContainerInterface;
    
    /**
     * @param string $key
     * @param $value
     *
     * @return ContainerInterface
     */
    public function setConfig( string $key, $value ): ContainerInterface;
    
    /**
     * @param string $key
     *
     * @return mixed
     * @throws InvalidConfigException
     */
    public function getConfig( string $key );
    
    /**
     * @param string $className
     * @param string $methodName
     * @param array $args
     *
     * @return mixed
     */
    public function call( string $className, string $methodName, array $args = [] );
    
    public function getClassMap(): ClassMapInterface;
    
    public function getMethodArguments(): ArgumentMapInterface;
    
    public function getResolvers(): ResolverMapInterface;
}