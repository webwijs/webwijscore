<?php

namespace Webwijs\Container\Exception;

use Exception;
use Throwable;

/**
 * Class InvalidServiceException to throw when a requested service is not of the required class
 * @package Webwijs\Auth\Exception
 */
class InvalidServiceException extends Exception
{
	/**
	 * InvalidServiceException constructor.
	 *
	 * @param string $message
	 * @param int $code
	 * @param Throwable|null $previous
	 */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct( $message, $code, $previous );
    }
}