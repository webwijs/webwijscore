<?php

namespace Webwijs\Container\Exception;

use Exception;
use Throwable;

/**
 * Class UnspecifiedArgumentException to throw when a requested argument is not specified
 * @package Webwijs\Auth\Exception
 */
class UnspecifiedArgumentException extends Exception
{
	/**
	 * UnspecifiedArgumentException constructor.
	 *
	 * @param string $message
	 * @param int $code
	 * @param Throwable|null $previous
	 */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct( $message, $code, $previous );
    }
}