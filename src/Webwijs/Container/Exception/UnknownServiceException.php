<?php

namespace Webwijs\Container\Exception;

use Exception;
use Throwable;

/**
 * Class UnknownServiceException to throw when a requested service does not exist
 * @package Webwijs\Auth\Exception
 */
class UnknownServiceException extends Exception
{
	/**
	 * UnknownServiceException constructor.
	 *
	 * @param string $message
	 * @param int $code
	 * @param Throwable|null $previous
	 */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct( $message, $code, $previous );
    }
}