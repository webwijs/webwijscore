<?php

namespace Webwijs\Container\Exception;

use Exception;

/**
 * Class UnknownResolverException to throw when a Request does not contain the right parameters
 * @package Webwijs\Auth\Exception
 */
class UnknownResolverException extends Exception
{
}