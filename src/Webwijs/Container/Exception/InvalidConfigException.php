<?php

namespace Webwijs\Container\Exception;

use Exception;
use Throwable;

/**
 * Class InvalidConfigException to throw when a requested config item is invalid or non-existent
 * @package Webwijs\Auth\Exception
 */
class InvalidConfigException extends Exception
{
	/**
	 * InvalidConfigException constructor.
	 *
	 * @param string $message
	 * @param int $code
	 * @param Throwable|null $previous
	 */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct( $message, $code, $previous );
    }
}