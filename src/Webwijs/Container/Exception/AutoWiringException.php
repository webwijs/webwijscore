<?php

namespace Webwijs\Container\Exception;

use Exception;
use Throwable;

/**
 * Class AutoWiringException to throw when auto wiring has failed
 * @package Webwijs\Auth\Exception
 */
class AutoWiringException extends Exception
{
	/**
	 * AutoWiringException constructor.
	 *
	 * @param string $message
	 * @param int $code
	 * @param Throwable|null $previous
	 */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct( $message, $code, $previous );
    }
}