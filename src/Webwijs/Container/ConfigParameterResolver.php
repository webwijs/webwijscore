<?php


namespace Webwijs\Container;


use ReflectionParameter;
use Webwijs\Container\Exception\InvalidConfigException;
use Webwijs\Container\Exception\UnresolvableParameterException;

class ConfigParameterResolver extends HardWiredParameterResolver
{
    public function resolve( ContainerInterface $container, ReflectionParameter $reflectionParameter )
    {
        try {
            $arg = parent::resolve( $container, $reflectionParameter );
            if ( @substr( $arg, 0, 1 ) === '#' ) {
                return $container->getConfig( ltrim( $arg, '#' ) );
            }
        } catch ( InvalidConfigException $e ) {
        }
        
    
        throw new UnresolvableParameterException( sprintf( 'Unable to hard-wire parameter "%s" for "%s::%s" from config',
            $reflectionParameter->getName(),
            $reflectionParameter->getDeclaringClass(),
            $reflectionParameter->getDeclaringFunction()
        ) );
    }
}