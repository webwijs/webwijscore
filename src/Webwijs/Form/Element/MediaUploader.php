<?php

namespace Webwijs\Form\Element;

use Webwijs\Form\Element;

class MediaUploader extends Element
{
    public $attribs = array('class' => 'media-uploader');
    public $helper = 'mediaUploader';
}
