<?php

namespace Webwijs\Form\Element;

use Webwijs\Form\Element;

class Color extends Element
{
    public $attribs = array('class' => 'color-picker');
    public $helper = 'text';
    
}
