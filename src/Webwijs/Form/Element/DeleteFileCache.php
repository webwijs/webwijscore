<?php

namespace Webwijs\Form\Element;

use Webwijs\Form\Element;

/**
 * A button to compile SCSS
 */
class DeleteFileCache extends Element
{
    public $attribs = array('class' => 'button-primary');
    public $helper = 'deleteFileCache';

    public function __construct($name, $options = null)
    {
        parent::__construct($name, $options);
        
        if(is_admin()) {
			      add_action('admin_enqueue_scripts', array($this, 'enqueueScripts'));
		    }
    }

    /**
     * Enqueue necessary scripts into the head of the admin page.
     *
     * @param string $hook identifies a page, which can be used to target a specific admin page.
     * @link http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
     */
    public function enqueueScripts()
    {
        wp_enqueue_script('delete-file-cache', get_bloginfo('stylesheet_directory') . '/assets/lib/js/admin/delete-file-cache.js', array('jquery'), false, true);
    }
}
