<?php
namespace Webwijs\View\Helper;

use Webwijs\System\Device;

use Webwijs\Http\Request;

class IsTablet
{
	public function isTablet()
	{
		$device = new Device(new Request());
        return $device->isTablet();
	}
}
?>
