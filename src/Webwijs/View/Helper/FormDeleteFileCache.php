<?php

namespace Webwijs\View\Helper;

class FormDeleteFileCache extends FormElement
{
    public function formDeleteFileCache($name, $value, $attribs, $options)
    {
        return '<button class="button-secondary" type="button" name="delete-file-cache" id="delete-file-cache"><span><span>Verwijder cache!</span></span></button>';
    }
}
