<?php

namespace Webwijs\View\Helper;
use Webwijs\Util\Arrays;

class FormInput extends FormElement
{
    public function formInput($name, $value, $attribs = array(), $options = array())
    {
		$attr = array(
			'type' => 'text',
			'value' => $value,
			'name' => $name,
			'class' => 'regular-text'
		);
		$args = Arrays::addAll($attr, (array) $attribs);
		$args['value'] = $this->escape($args['value']);

        !isset($args['id']) && $args['id'] = $args['name'] . '-input';
        return '<input' . $this->_renderAttribs($args) . '/>';
    }
}
