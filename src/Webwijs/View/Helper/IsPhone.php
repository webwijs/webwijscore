<?php
namespace Webwijs\View\Helper;

use Webwijs\System\Device;

use Webwijs\Http\Request;

class IsPhone
{
	public function isPhone()
	{
		$device = new Device(new Request());
        return ( $device->isPhone() || $device->isSmartphone() ) ? true : false;
	}
}
?>
