<?php

namespace Webwijs\View\Helper;

class FormMediaUploader extends FormElement
{
	/**
	 * Renders a form element to pick an image from the library.
	 *
	 * You can limit the filetypes that are allowed by passing 'type' as an option in the options array.
	 * the first word of a mimetype allows all files of that type, specifying a full mimetype limits to only that filetype
	 *
	 * $options = ['type' => 'image']
	 * the media library shows all images
	 *
	 * $options = ['type' => 'image/jpeg']
	 * the media library only shows jpeg images
	 *
	 * @param  string  $name    the name of the field
	 * @param  integer $value   the value of the field (attachment id)
	 * @param  array   $attribs the attributes of the form element
	 * @param  array   $options other options
	 * @return string    		the rendered HTML of the form element
	 */
    public function formMediaUploader($name, $value, $attribs = array(), $options = array())
    {
        $attribs['value'] = $this->escape($value);
        $attribs['name'] = $name;
        !isset($attribs['id']) && $attribs['id'] = $name . '-input';
		$attribs['mime'] = get_post_mime_type($value);
		!isset($options['type']) && $options['type'] = '';
		return $this->view->partial('partials/admin/form/element/media-uploader.phtml', array('attribs' => $attribs, 'opts' => $options));
    }
}
