<?php

namespace Webwijs\File;

use SplFileInfo;

/**
 * The NullFilter performs neutral ("null") behavior by accepting all files and directories.
 *
 * @author Leo Flapper
 * @version 1.1.0
 * @since 1.0.0
 */
class NullFilter implements FilterInterface
{
	/**
	 * Returns true for all directories and files.
	 *
	 * @param  SplFileInfo $file the Spl File Info object
	 * @return true to accept all directories and files.
	 */
	public function accept(SplFileInfo $file)
	{
		return true;
	}
}
