<?php


namespace Webwijs;

use Webwijs\Container\ContainerInterface;

interface ThemeInterface
{
    public function init();
    
    public function configure();
}