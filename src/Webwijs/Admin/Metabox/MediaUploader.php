<?php

namespace Webwijs\Admin\Metabox;

use Webwijs\Admin\AbstractMetabox;
use Webwijs\View;

class MediaUploader extends AbstractMetabox
{
    /**
     * settings used by the metabox.
     *
     * @var array
     */
    protected $settings = array(
        'id'       => 'meta_media_uploader',
        'title'    => 'Media',
        'context'  => 'side',
        'priority' => 'low',
    );

    /**
     * Additional options used to display the metabox.
     *
     * @var array
     */
    public $options = array();

    /**
     * Method which will be called once the metabox has been created.
     *
     * @return void
     */
    public function init()
    {
        $defaults = array(
            'name' => 'media_uploader',
            'class' => 'widefat',
            'description' => '',
        );
        $this->options = array_merge($defaults, (array) $this->options);
    }

    /**
     * Display a form or other html elements which can be used associate meta data with
     * a particular post.
     *
     * @param \WP_Post $post the post object which is currently being displayed.
     */
    public function display($post)
    {
		$key = $this->getName($this->options['name']);
        $value = $this->getPostMeta($post->ID, $this->options['name'], true);
		$view = new View();
		echo $view->formMediaUploader($key, $value, $this->settings, $this->options);
    ?>
        <?php if (!empty($this->options['description'])): ?>
        <p><?php echo esc_attr($this->options['description']) ?></p>
        <?php endif ?>
    <?php
    }

    /**
     * Allows the meta data entered on the admin page to be saved with a
     * particular post.
     *
     * @param int $postId the ID of the post that the user is editing.
     */
    public function save($postId)
    {
		$name = $this->options['name'];
		$value = $this->getPostValue($name);
        $this->updatePostMeta($postId, $name, $value);
    }

    /**
     * Set options for this metabox; which include but are limited to
     * a classname and description.
     *
     * @param array $options associative array containing options.
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }
}
