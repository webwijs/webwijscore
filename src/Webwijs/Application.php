<?php

namespace Webwijs;

use Doctrine\Common\Cache\CacheProvider;
use Webwijs\Action\FlatUrl;
use Webwijs\Action\MySQL;
use Webwijs\Action\NestedUrl;
use Webwijs\Action\NotFound;
use Webwijs\Container\ActionManagerInterface;
use Webwijs\Container\ContainerInterface;
use Webwijs\Filter\CustomQuery;
use Webwijs\Filter\Seo;
use Webwijs\Loader\ClassLoader;
use Webwijs\Cache\CacheManager;
use Webwijs\Cache\Loader\RedisLoader;
use Webwijs\Cache\Loader\XCacheLoader;
use Webwijs\Cache\Loader\MemcachedLoader;
use Webwijs\Cache\Loader\FileCacheLoader;
use Webwijs\Module\Action\ModuleLoaded;
use Webwijs\Module\ModuleOptions;
use Webwijs\Module\ModuleManager;
use Webwijs\Template\Loader;
use Webwijs\Template\PageTemplates;

class Application
{
    public static $serviceManager;
    
    public static $modelManager;
    
    private $cacheManager;
    
    /**
     * @var ContainerInterface|null
     */
    private $container;
    
    /**
     * @var ActionManagerInterface|null
     */
    private $actionManager;
    
    /**
     * Application constructor.
     *
     * @param ContainerInterface|null $container
     * @param ActionManagerInterface|null $actionManager
     */
    public function __construct( ContainerInterface $container = null, ActionManagerInterface $actionManager = null )
    {
        $this->container     = $container;
        $this->actionManager = $actionManager;
    }
    
    
    public function init()
    {
        foreach ( get_class_methods( $this ) as $method ) {
            if ( strpos( $method, '_init' ) === 0 ) {
                $this->$method();
            }
        }
    }
    
    /**
     * Add static resources to the class loader.
     */
    protected function _initResourceloaders()
    {
        ClassLoader::addStaticResources( [
            'viewhelper'        => 'Webwijs\View\Helper',
            'formdecorator'     => 'Webwijs\Form\Decorator',
            'formelement'       => 'Webwijs\Form\Element',
            'validator'         => 'Webwijs\Validate',
            'facetsearch'       => 'Webwijs\FacetSearch',
            'facetsearchfilter' => 'Webwijs\FacetSearch\Filter',
            'model'             => 'Webwijs\Model',
            'modeltable'        => 'Webwijs\Model\Table',
            'service'           => 'Webwijs\Service',
        ] );
    }
    
    protected function _initSetup()
    {
        $this->actionManager->addAction( 'after_switch_theme', MySQL::class, 'setupRelatedPosts' );
//        add_action( 'after_switch_theme', [ 'Webwijs\Action\MySQL', 'setupRelatedPosts' ] );
    }
    
    /**
     * Starts the session
     * @return void
     */
    protected function _initSession()
    {
//        if ( empty( get_option( 'theme_advanced_varnish' ) ) ) {
//            //session_start();
//        }
    }
    
    protected function _initUrls()
    {
        if ( get_option( 'theme_advanced_flat_url' ) ) {
//            add_action( 'post_type_link', [ 'Webwijs\Action\FlatUrl', 'createCustomPermalink' ], 10, 100 );
//            add_action( 'page_link', [ 'Webwijs\Action\FlatUrl', 'createPagePermalink' ], 10, 100 );
//            add_action( 'parse_request', [ 'Webwijs\Action\FlatUrl', 'parseRequest' ] );
            $this->actionManager
                ->addAction( 'post_type_link', FlatUrl::class, 'createCustomPermalink', 10, 100 )
                ->addAction( 'page_link', FlatUrl::class, 'createPagePermalink', 10, 100 )
                ->addAction( 'parse_request', FlatUrl::class, 'parseRequest' )
            ;
        } else {
//            add_filter( 'term_link', [ 'Webwijs\Action\NestedUrl', 'createTermLink' ], 10, 3 );
//            add_action( 'parse_request', [ 'Webwijs\Action\NestedUrl', 'parseRequest' ] );
//            add_filter( 'get_pagenum_link', [ 'Webwijs\Action\NestedUrl', 'pagenumLink' ] );
//            add_filter( 'redirect_canonical', [ 'Webwijs\Action\NestedUrl', 'redirectCanonical' ], 10, 2 );
//            add_filter( 'year_link', [ 'Webwijs\Action\NestedUrl', 'yearLink' ], 10, 2 );
//            add_filter( 'month_link', [ 'Webwijs\Action\NestedUrl', 'monthLink' ], 10, 3 );
            $this->actionManager
                ->addAction( 'parse_request', NestedUrl::class, 'parseRequest' )
                ->addFilter( 'term_link', NestedUrl::class, 'createTermLink', 10, 3 )
                ->addFilter( 'get_pagenum_link', NestedUrl::class, 'pagenumLink' )
                ->addFilter( 'redirect_canonical', NestedUrl::class, 'redirectCanonical', 10, 2 )
                ->addFilter( 'year_link', NestedUrl::class, 'yearLink', 10, 2 )
                ->addFilter( 'month_link', NestedUrl::class, 'monthLink', 10, 3 )
            ;
        }
    }
    
    protected function _initErrorPage()
    {
//        add_action( 'wp', [ 'Webwijs\Action\NotFound', 'query404' ], 10, 1 );
//        add_filter( 'redirect_canonical', [ 'Webwijs\Action\NotFound', 'stopRedirect' ], 10, 2 );
//        add_filter( '404_template', [ 'Webwijs\Action\NotFound', 'locateTemplate' ], 10, 1 );
        $this->actionManager
            ->addAction( 'wp', NotFound::class, 'query404', 10, 1 )
            ->addFilter( 'redirect_canonical', NotFound::class, 'stopRedirect', 10, 2 )
            ->addFilter( '404_template', NotFound::class, 'locateTemplate', 10, 1 )
        ;
    }
    
    protected function _initSeo()
    {
//        add_filter( 'wpseo_sitemap_exclude_taxonomy', [ 'Webwijs\Filter\Seo', 'excludeTaxonomy' ], 10, 2 );
        $this->actionManager->addFilter('wpseo_sitemap_exclude_taxonomy', Seo::class, 'excludeTaxonomy', 10, 2 );
    }
    
    /**
     * Initiates the module manager for loading the modules
     */
    protected function _initModules()
    {
//        add_action( 'module_loaded', [ 'Webwijs\Module\Action\ModuleLoaded', 'addResources' ] );
//        add_action( 'module_loaded', [ 'Webwijs\Module\Action\ModuleLoaded', 'setModule' ], 10, 2 );
//        add_action( 'module_loaded', [ 'Webwijs\Module\Action\ModuleLoaded', 'loadModulePageTemplates' ], 99 );
//        add_action( 'module_loaded', [ 'Webwijs\Module\Action\ModuleLoaded', 'initModule' ], 99, 2 );
        $this->actionManager
            ->addAction('module_loaded', ModuleLoaded::class, 'addResources' )
            ->addAction('module_loaded',  ModuleLoaded::class, 'setModule', 10, 2 )
            ->addAction('module_loaded', ModuleLoaded::class, 'loadModulePageTemplates', 99 )
            ->addAction('module_loaded', ModuleLoaded::class, 'initModule', 99, 2 )
        ;
        
        $moduleOptions = new ModuleOptions();
        $moduleOptions->setCacheProvider( $this->getCacheProvider() );
        
        $moduleManager = new ModuleManager( $moduleOptions, $this->container, $this->actionManager );
        $moduleManager->loadModules( get_template_directory() . '/modules' );
    }
    
    /**
     * Returns the cache provider to use for caching the module routes
     * @return CacheProvider the cache driver to use
     */
    public function getCacheProvider()
    {
        if ( $this->cacheManager === null ) {
            $this->cacheManager = $this->createCacheProvider();
        }
        
        return $this->cacheManager->getCacheProvider();
    }
    
    /**
     * Create a CacheManager instance.
     *
     * @return CacheManager The CacheManager object.
     */
    private function createCacheProvider()
    {
        /** @var CacheManager $cacheManager */
        $cacheManager = $this->container->get( CacheManager::class );

//        $cacheManager = new CacheManager();
        $cacheManager->addLoaders( [
            $this->container->get( MemcachedLoader::class ),
            $this->container->get( XCacheLoader::class ),
        ] );
        
        // enable doctrine file cache if WP_DEBUG is false
        if ( defined( 'WP_DEBUG' ) && false === WP_DEBUG ) {
            $cacheManager->addLoader( $this->container->get( FileCacheLoader::class ) );
        }
        
        if ( ! empty( get_option( 'theme_advanced_redis_cache' ) ) ) {
            $cacheManager->addLoader( $this->container->get( RedisLoader::class ) );
        }
        
        return $cacheManager;
    }
    
    /**
     * Adds custom page templates to the wordpress page templates
     * @return void
     */
    protected function _initPageTemplates()
    {
//        add_filter( 'theme_page_templates', [ 'Webwijs\Template\PageTemplates', 'applyTemplates' ] );
        $this->actionManager->addFilter( 'theme_page_templates', PageTemplates::class, 'applyTemplates' );
    }
    
    /**
     * Adds the template loader to the template redirect action
     * @return void
     */
    protected function _initTemplateLoader()
    {
//        add_action( 'template_redirect', [ 'Webwijs\Template\Loader', 'load' ], 1000 );
        $this->actionManager->addAction( 'template_redirect', Loader::class, 'load', 1000 );
    }
    
    /**
     * Adds the custom Webwijs query to the post clauses filter
     * @return void
     */
    protected function _initWpQuery()
    {
//        add_filter( 'posts_clauses', [ 'Webwijs\Filter\CustomQuery', 'filter' ], 10, 2 );
        $this->actionManager->addFilter( 'posts_clauses', CustomQuery::class, 'filter', 10, 2 );
    }
    
    /**
     * Returns the ServiceManager instance for maintaining services.
     *
     * @return ServiceManager The service manager.
     */
    public static function getServiceManager()
    {
        if ( is_null( self::$serviceManager ) ) {
            self::$serviceManager = new ServiceManager();
        }
        
        return self::$serviceManager;
    }
    
    /**
     * Returns the ModelManager instance for maintaining database models.
     *
     * @return ModelManager The model manager.
     */
    public static function getModelManager()
    {
        if ( is_null( self::$modelManager ) ) {
            self::$modelManager = new ModelManager();
        }
        
        return self::$modelManager;
    }
}
