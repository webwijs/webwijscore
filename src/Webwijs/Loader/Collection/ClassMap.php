<?php

namespace Webwijs\Loader\Collection;

/**
 * The ClassMap maintains a table of fully qualified class names and their absolute path on the filesystem.
 */
class ClassMap implements ClassMapInterface
{
    /**
     * The mapping between classes and path names.
     *
     * @var string[] $directories
     */
    private $directories;

    /**
     * Initialize a new ClassMap.
     *
     * @param array $classNames (optional) The class names to populate this map with.
     */
    public function __construct(array $classNames = [])
    {
        $this->putAll($classNames);
    }

    /**
     * Associate the specified class name with the specified path in this map.
     *
     * @param string $className The fully qualified class name.
     * @param string $directory The absolute path name of the specified class.
     * @throws \InvalidArgumentException If the class name is not a string argument.
     * @throws \InvalidArgumentException If the path is not a string argument.
     */
    public function put($className, $directory)
    {
        $this->ensureString($className);
        $this->ensureString($directory);

        $this->directories[$className] = $directory;
    }

    /**
     * Associate all the class names and path in the specified collection into this map.
     *
     * @param string[] $classNames A collection of key-value pairs.
     */
    public function putAll(array $classNames)
    {
        foreach ($classNames as $className => $directory) {
            $this->put($className, $directory);
        }
    }

    /**
     * Associate if not yet present the specified class name with the specified path in this map.
     *
     * @param string $className The fully qualified class name.
     * @param string $directory The absolute path name to associated with the specified class.
     * @return bool True if the specified class name dit not yet exist and was added to this map.
     */
    public function putIfAbsent($className, $directory)
    {
        $absent = ($this->containsKey($className) === false);

        if ($absent) {
            $this->put($className, $directory);
        }

        return $absent;
    }

    /**
     * Returns a collection of fully qualified class name held by this map.
     *
     * @return string[] A collection of fully qualified class names.
     */
    public function classNames()
    {
        return array_keys($this->directories);
    }

    /**
     * Returns true if this map contains a mapping for the specified class name.
     *
     * @param string $className The fully qualified class name whose presence will be tested.
     * @return bool True if the specified class name is contained within this map.
     */
    public function containsKey($className)
    {
        return isset($this->directories[$className]);
    }

    /**
     * Returns true if this map contains a mapping for the specified path.
     *
     * @param string $directory The path whose presence will be tested.
     * @return bool True if the specified path is contained within this map.
     */
    public function contains($directory)
    {
        return in_array($directory, $this->directories, true);
    }

    /**
     * Returns if present the path associated with the specified fully qualified class name.
     *
     * @param string $className The fully qualified class name.
     * @param null|string $default (optional) The default value to return, defaults to null.
     * @return null|string The path associated with the specified class name, or default value on failure.
     */
    public function get($className, $default = null)
    {
        $directory = $default;

        if ($this->containsKey($className)) {
            $directory = $this->directories[$className];
        }

        return $directory;
    }

    /**
     * Returns true if this map contains no key-value mappings.
     *
     * @return bool True if this map is considered to be empty.
     */
    public function isEmpty()
    {
        return empty($this->directories);
    }

    /**
     * Returns a collection of absolute path names held by this map.
     *
     * @return string[] A collection of path names.
     */
    public function paths()
    {
        return array_values($this->directories);
    }

    /**
     * Remove the path name associated with the specified class name from this map.
     *
     * @param string $className The fully qualified class name to remove from this map.
     * @return null|string The path name that was removed from this map, or null on failure.
     */
    public function remove($className)
    {
        $directory = $this->get($className);

        if ($this->containsKey($className)) {
            unset($this->directories[$className]);
        }

        return $directory;
    }

    /**
     * Remove the path names associated with the specified collection of class names from this map.
     *
     * @param string[] $classNames The fully qualified class names to remove from this map.
     */
    public function removeAll(array $classNames)
    {
        foreach ($classNames as $className) {
            if ($this->containsKey($className)) {
                unset($this->directories[$className]);
            }
        }
    }

    /**
     * Replace the path name associated with the specified class name with a new path name.
     *
     * @param string $className The fully qualified class name whose path name to replace.
     * @param string $directory The path name to replace the previous path name with.
     * @return null|string The previous path name associated with the specified class name.
     */
    public function replace($className, $directory)
    {
        $oldDirectory = $this->get($className);

        if ($this->containsKey($className)) {
            $this->put($className, $directory);
        }

        return $oldDirectory;
    }

    /**
     * Replace each path name associated with the specified collection of path names.
     *
     * @param string[] $classNames A collection of key-value pairs.
     */
    public function replaceAll(array $classNames)
    {
        foreach ($classNames as $className => $directory) {
            if ($this->containsKey($className)) {
                $this->put($className, $directory);
            }
        }
    }

    /**
     * Returns an external iterator for the fully qualified class named contained within this map.
     *
     * @return \Iterator An iterator for the fully qualified class named held by this map.
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->directories);
    }

    /**
     * Returns the number of fully qualified class names that are contained within this map.
     *
     * @return int The number of fully qualified class name held by this map.
     */
    public function count()
    {
        return count($this->directories);
    }

    /**
     * Ensure that the specified argument is a string.
     * 
     * @param mixed $value The value that will be tested.
     * @throws \InvalidArgumentException If the specified argument is not a string.
     */
    private function ensureString($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf(
                'Expects the given argument to be a string; received %s instead.',
                (is_object($value)) ? get_class($value) : gettype($value)
            ));
        }
    }
}