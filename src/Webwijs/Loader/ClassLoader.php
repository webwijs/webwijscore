<?php

namespace Webwijs\Loader;

use Webwijs\Loader\Collection\ClassMap;
use Webwijs\Loader\Psr4Autoloader as Autoloader;
use Webwijs\Util\Strings;

class ClassLoader
{
    /**
     * A mapping between classes and file names.
     *
     * @var null|ClassMap $classMap
     */
    private static $classMap;

    /**
     * A collection of resources to seed all loaders with.
     *
     * @var string[][] $staticResources
     */
    private static $staticResources = array();

    /**
     * A collection of resources specific to this loader.
     *
     * @var string[][] $resources
     */
    private $resources = array();

    /**
     * initialize a new ClassLoader.
     *
     * @param array|\Traversable $resources (optional) The resource types and namespaces to register.
     */
    public function __construct($resources = [])
    {
        $this->registerResources(self::$staticResources);
        $this->registerResources($resources);
    }

    /**
     * Register one or more resources with the loader.
     *
     * @param array|\Traversable $resources one or more resources to register.
     * @throws \InvalidArgumentException if the provided argument is not an array or instance of Traversable.
     */
    public function registerResources($resources)
    {
        self::ensureTraversable($resources);

        foreach ($resources as $shortName => $namespace) {
            $this->registerResource($shortName, $namespace);
        }
    }

    /**
     * Register a new namespace with the specified resource type.
     *
     * @param string $resourceType The resource type to which the specified namespace will be added.
     * @param string $namespace The namespace to register with the specified resource type.
     * @throws \InvalidArgumentException if either one of the arguments is not of type string.
     */
    public function registerResource($resourceType, $namespace)
    {
        self::ensureString($resourceType);
        self::ensureString($namespace);

        $offset = strtolower($resourceType);

        $this->resources[$offset][] = $namespace;
    }

    /**
     * Removes all namespaces associated with the specified resource type.
     *
     * @param string $resourceType The resource type whose namespace(s) to remove.
     * @throws \InvalidArgumentException If the given argument is not a string.
     */
    public function unregisterResource($resourceType)
    {
        self::ensureString($resourceType);

        $offset = strtolower($resourceType);

        if (isset($this->resources[$offset])) {
            unset($this->resources[$offset]);
        }
    }

    /**
     * Returns an ordered collection of registered namespaces.
     *
     * @return string[] A collection of namespaces.
     */
    public function getRegisteredResources()
    {
        $allResources = array();

        foreach ($this->resources as $resource) {
            $allResources = array_merge($allResources, $resource);
        }

        return $allResources;
    }

    /**
     * Returns true if this loader holds namespaces for the specified resource type.
     *
     * @param string $resourceType The resource type whose presence will be tested.
     * @return bool True if the specified resource type was found.
     * @throws \InvalidArgumentException if the given argument is not of type string.
     */
    public function isRegisteredResource($resourceType)
    {
        self::ensureString($resourceType);

        $offset = strtolower($resourceType);

        return (isset($this->resources[$offset]));
    }

    /**
     * Returns if present the (first) fully qualified class name for the specified resource type.
     *
     * @param string $resourceType the type of resource to search in.
     * @param string $className the name of a class to find.
     * @return string|null The fully qualified class name, or null on failure.
     * @throws \InvalidArgumentException if either one of the arguments is not of type string.
     */
    public function load($resourceType, $className)
    {
        self::ensureString($resourceType);
        self::ensureString($className);

        $map = self::getClassMap();
        
        $classKey = "{$resourceType}\\{$className}";

        if ($map->containsKey($classKey)) {
            return $map->get($classKey);
        }

        $offset = strtolower($resourceType);

        if (isset($this->resources[$offset])) {
            $namespaces = array_reverse($this->resources[$offset]);

            foreach ($namespaces as $namespace) {
                if ($class = self::getClassByName($className, $namespace)) {
                    $map->put($classKey, $class);
                    break;
                }
            }

            $map->putIfAbsent($classKey, '');
        }

        return $map->get($classKey);
    }
    
    /**
     * Register one or more global resources.
     *
     * @param array|\Traversable $resources one or more resources to register.
     * @throws \InvalidArgumentException if the provided argument is not an array or instance of Traversable.
     */
    public static function addStaticResources($resources)
    {        
        self::ensureTraversable($resources);
        
        foreach ($resources as $shortName => $namespace) {
            self::addStaticResource($shortName, $namespace);
        }
    }
    
    /**
     * Register a new namespace with the specified resource type.
     *
     * @param string $resourceType The resource type to which the specified namespace will be added.
     * @param string $namespace The namespace to register with the specified resource type.
     * @throws \InvalidArgumentException if either one of the arguments is not of type string.
     */
    public static function addStaticResource($resourceType, $namespace)
    {
        self::ensureString($resourceType);
        self::ensureString($namespace);
        
        $offset = strtolower($resourceType);

        self::$staticResources[$offset][] = $namespace;
    }
    
    /**
     * Removes all global resources. The array containing these resources will be empty
     * after this call returns.
     *
     * @return void
     */
    public static function clearStaticResources()
    {
        self::$staticResources = array();
    }
    
    /**
     * Removes all namespaces associated with the specified resource type.
     *
     * @param string $resourceType The resource type whose namespace(s) to remove.
     * @throws \InvalidArgumentException If the given argument is not a string.
     */
    public static function removeStaticResource($resourceType)
    {
        self::ensureString($resourceType);

        $offset = strtolower($resourceType);

        if (isset(self::$staticResources[$offset])) {
            unset(self::$staticResources[$offset]);
        }
    }
    
    /**
     * Returns if present the (first) fully qualified class name for the specified resource type.
     *
     * @param string $resourceType the type of resource to search in.
     * @param string $className the name of a class to find.
     * @return string|null The fully qualified class name, or null on failure.
     * @throws \InvalidArgumentException if either one of the arguments is not of type string.                   
     */
    public static function loadStatic($resourceType, $className)
    {    
        self::ensureString($resourceType);
        self::ensureString($className);

        $classMap = self::getClassMap();
        $classKey = "{$resourceType}\\{$className}";

        if ($classMap->containsKey($classKey)) {
            return $classMap->get($classKey);
        }

        $offset = strtolower($resourceType);

        if (isset(self::$staticResources[$offset])) {
            $namespaces = array_reverse(self::$staticResources[$offset]);

            foreach ($namespaces as $namespace) {
                if ($class = self::getClassByName($className, $namespace)) {
                    $classMap->put($classKey, $class);
                    break;
                }
            }

            $classMap->putIfAbsent($classKey, '');
        }

        return $classMap->get($classKey);
    }

    /**
     * Returns if present on the filesystem the fully qualified class name of the specified class.
     *
     * @param string $className The name of the class to find.
     * @param string|null $namespace (optional) The namespace that is prepended to the class name.
     * @return string|null The fully qualified class name, or null on failure.
     */
    private static function getClassByName($className, $namespace = null)
    {
        if ($namespace) {
            $className = Strings::addTrailing($namespace, Autoloader::NS_SEPARATOR) . $className;
        }

        return (class_exists($className)) ? $className : null;
    }

    /**
     * Returns the class map held by this autoloader.
     *
     * @return ClassMap The class map.
     */
    private static function getClassMap()
    {
        if (self::$classMap === null) {
            self::$classMap = new ClassMap();
        }

        return self::$classMap;
    }

    /**
     * Ensure that the specified argument is a string.
     *
     * @param mixed $value The value that will be tested.
     * @throws \InvalidArgumentException If the specified argument is not a string.
     */
    private static function ensureString($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf(
                'Expects the given argument to be a string; received %s instead.',
                (is_object($value)) ? get_class($value) : gettype($value)
            ));
        }
    }

    /**
     * Ensure that the specified argument is an array or \Traversable object.
     *
     * @param mixed $value The value that will be tested.
     * @throws \InvalidArgumentException If the specified argument is not traversable.
     */
    private static function ensureTraversable($value)
    {
        if (!is_array($value) && !($value instanceof \Traversable)) {
            throw new \InvalidArgumentException(sprintf(
                'Expects an array or instance of the Traversable; received "%s"',
                is_object($value) ? get_class($value) : gettype($value)
            ));
        }
    }
}
