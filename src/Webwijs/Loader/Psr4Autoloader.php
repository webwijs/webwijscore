<?php

namespace Webwijs\Loader;

use Webwijs\Loader\Collection\ClassMap;

require_once __DIR__ . '/Collection/ClassMapInterface.php';
require_once __DIR__ . '/Collection/ClassMap.php';

/**
 * The Psr4Autoloader is a PSR-4 compliant autoloader.
 *
 * @link https://www.php-fig.org/psr/psr-4/
 */
class Psr4Autoloader
{
    /**
     * The namespace separator.
     *
     * @var string NS_SEPARATOR
     */
    const NS_SEPARATOR = '\\';

    /**
     * The mapping between namespaces and absolute paths.
     *
     * @var string[] $namespaces
     */
    private $namespaces = [];

    /**
     * A mapping between classes and file names.
     *
     * @var null|ClassMap $classMap
     */
    private $classMap;

    /**
     * The pattern to match classes and interfaces with.
     *
     * @var string CLASS_PATTERN
     */
    const CLASS_PATTERN = '(?P<class>[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)';

    /**
     * The pattern to match namespaces with.
     *
     * @var string NS_PATTERN
     */
    const NS_PATTERN = '(?P<ns>[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*(?:\\\[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)*)';

    /**
     * Constructor
     *
     * Allow configuration of the autoloader via the constructor.
     *
     * @param  array|\Traversable $namespaces (optional) The mapping between namespaces and absolute paths.
     */
    public function __construct($namespaces = [])
    {
        $this->registerNamespaces($namespaces);
    }

    /**
     * Set the class map containing the mapping between classes and file names.
     *
     * @param string[] $classNames A collection of key-value pairs.
     */
    public function setClassMap(array $classNames)
    {
        $this->classMap = new ClassMap($classNames);
    }

    /**
     * Returns the class map held by this autoloader.
     *
     * @return ClassMap The class map.
     */
    private function getClassMap()
    {
        if ($this->classMap === null) {
            $this->classMap = new ClassMap();
        }

        return $this->classMap;
    }

    /**
     * Register the specified collection of namespaces with this autoloader.
     *
     * @param  string[] $namespaces The mapping between namespaces and absolute paths.
     * @return Psr4Autoloader An instance of this autoloader to allow method chaining.
     */
    public function registerNamespaces(array $namespaces)
    {
        foreach ($namespaces as $namespace => $pathName) {
            $this->registerNamespace($namespace, $pathName);
        }

        return $this;
    }

    /**
     * Register the specified namespace with this autoloader.
     *
     * @param string $namespace The namespace to register.
     * @param string $pathName The absolute path to associate with this namespace.
     */
    public function registerNamespace($namespace, $pathName)
    {
        $this->ensureString($pathName);
        $this->ensureString($namespace);

        $namespace = $this->normalizeNamespace($namespace);
        $directory = $this->normalizeDirectory($pathName);

        $this->namespaces[$namespace] = $directory;
    }

    /**
     * Returns the namespaces registered with this autoloader.
     *
     * @return string[] The namespaces held by this autoloader.
     */
    public function getNamespaces()
    {
        return $this->namespaces;
    }

    /**
     * Normalize the specified directory by ensuring that it ends with a directory separator.
     *
     * @param string $directory The directory to normalize.
     * @return string The normalized directory.
     */
    private function normalizeDirectory($directory)
    {
        $dirSeparators = ['\\', '/'];
        $lastCharIndex = (strlen($directory) - 1);

        if (in_array($directory[$lastCharIndex], $dirSeparators, true)) {
            $directory = substr($directory, 0, -1);
        }

        return rtrim($directory, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }

    /**
     * Normalize the specified namespace by ensuring that it ends with a namespace separator.
     *
     * @param string $namespace The namespace to normalize.
     * @return string The normalized namespace.
     */
    private function normalizeNamespace($namespace)
    {
        $lastCharIndex = (strlen($namespace) - 1);

        if ($namespace[$lastCharIndex] === self::NS_SEPARATOR) {
            $namespace = substr($namespace, 0, -1);
        }

        return rtrim($namespace, self::NS_SEPARATOR) . self::NS_SEPARATOR;
    }

    /**
     * Autoload the specified class.
     *
     * @param string $class The class to autoload.
     * @return string|bool The contents of the included file, or false on failure.
     */
    public function autoload($class)
    {
        $contents = false;
        $classMap = $this->getClassMap();

        if ($classMap->containsKey($class)) {
            $filename = $classMap->get($class);
        } else {
            $filename = $this->resolveFilename($class);
        }

        if ($filename) {
            $contents = include_once $filename;
        }

        return $contents;
    }

    /**
     * Resolve the absolute path for the specified class name.
     *
     * @param string $class The fully qualified class name to resolve.
     * @return string The resolve filename, or empty string on failure.
     */
    private function resolveFilename($class)
    {
        $resolvedFile = '';

        foreach ($this->namespaces as $namespace => $pathName) {
            if (0 === strpos($class, $namespace)) {
                // Remove namespace from the class.
                $className = substr($class, strlen($namespace));

                // Create filename
                $filename = $this->transformClassNameToFilename($className, $pathName);

                if (file_exists($filename)) {
                    $resolvedFile = $filename;
                }

                break;
            }
        }

        return $resolvedFile;
    }

    /**
     * Register the autoloader with spl_autoload registry
     *
     * @return void
     */
    public function register()
    {
        spl_autoload_register(array($this, 'autoload'));
    }

    /**
     * Transform the specified class name into filename.
     *
     * @param string $className The class name to transform.
     * @param string $pathName The path that proceeds the specified class name.
     * @return string The filename to which the specified class name belongs.
     */
    private function transformClassNameToFilename($className, $pathName)
    {
        $classNamePattern = static::CLASS_PATTERN;
        $namespacePattern = static::NS_PATTERN;
        $pattern = "/(?:{$namespacePattern}\\\)?{$classNamePattern}$/";

        preg_match($pattern, $className, $matches);

        $className = (isset($matches['class'])) ? $matches['class'] : '';
        $namespace = (isset($matches['ns'])) ? $matches['ns'] : '';

        $segments = explode(self::NS_SEPARATOR, $namespace);
        $segments[] = $className;

        return $pathName . sprintf('%s.php', join(DIRECTORY_SEPARATOR, $segments));
    }

    /**
     * Ensure that the specified argument is a string.
     *
     * @param mixed $value The value that will be tested.
     * @throws \InvalidArgumentException If the specified argument is not a string.
     */
    private function ensureString($value)
    {
        if (!is_string($value)) {
            throw new \InvalidArgumentException(sprintf(
                'Expects the given argument to be a string; received %s instead.',
                (is_object($value)) ? get_class($value) : gettype($value)
            ));
        }
    }
}