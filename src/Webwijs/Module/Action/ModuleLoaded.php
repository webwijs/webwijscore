<?php

namespace Webwijs\Module\Action;

use Webwijs\AbstractBootstrap;
use Webwijs\File\FileFinder;
use Webwijs\Loader\ClassLoader;
use Webwijs\Module\Finder\TemplateFilter;
use Webwijs\View\Directories as ViewDirectories;
use Webwijs\Template\PageTemplates;
use Webwijs\Module\ModuleAwareInterface;
use Webwijs\Module\ModuleInfo;

/**
 * Module Loaded
 *
 * Contains the methods which are called after a module is loaded
 *
 * @author Leo Flapper <leo@webwijs.nu>
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.1.0
 * @since 1.1.0
 */
class ModuleLoaded
{	

    /**
     * Adds the module helpers to the helper resources and the views to the view directories.
     * The view object can use these to display the right helper and view.
     * @param ModuleInfo $moduleInfo the module info
     */
	public static function addResources(ModuleInfo $moduleInfo)
	{
		ClassLoader::addStaticResources(array(
            'viewhelper' => $moduleInfo->getNamespace().'\Helper'
        ));

		ViewDirectories::addStaticDirectory($moduleInfo->getPath());
	}

    /**
     * Sets the module info class in the bootstrap if the bootstrap contains
     * the Module Aware Interface.
     *
     * @param ModuleInfo $moduleInfo the module info
     * @param AbstractBootstrap $bootstrap the module it's bootstrap class
     */
    public static function setModule(ModuleInfo $moduleInfo, AbstractBootstrap $bootstrap)
    {
        if ($bootstrap instanceof ModuleAwareInterface) {
            $bootstrap->setModule($moduleInfo);
        }   
    }
    
    /**
     * Initializes the module bootstrap
     * 
     * @param ModuleInfo $moduleInfo the module info
     * @param AbstractBootstrap $bootstrap the module it's bootstrap class
     * @return void
     */
    public static function initModule(ModuleInfo $moduleInfo, AbstractBootstrap $bootstrap)
    {
        $bootstrap->init();
    }

    /**
     * Loads the page templates of the module
     * 
     * @param ModuleInfo $moduleInfo the module info
     * @return void
     */
    public static function loadModulePageTemplates(ModuleInfo $moduleInfo)
    {
        $fileFinder = new FileFinder(\RecursiveDirectoryIterator::SKIP_DOTS);
        $fileFinder->addPath($moduleInfo->getPath());
        $fileFinder->setMaxDepth(0);
        $fileFinder->setFilter(new TemplateFilter());

        foreach ($fileFinder as $fileInfo) {
            PageTemplates::setPageTemplate($fileInfo, sprintf('Module %s - ', $moduleInfo->getName()));
        }
    }  

}
