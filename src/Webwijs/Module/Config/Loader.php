<?php

namespace Webwijs\Module\Config;

use Webwijs\Util\Strings;

/**
 * Config loader
 *
 * Loads the config data for the modules
 *
 * @author Leo Flapper <leo@webwijs.nu>
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.1.0
 * @since 1.1.0
 */
class Loader 
{
	/**
	 * Loads the configuration file
	 * @param  string $moduleName the name of the module.
	 * @param  string $modulePath the absolute path to the module.
	 * @return \SimpleXmlElement Object containing the configuration data
	 * @throws \InvalidArgumentException If the given arguments are not string types.
	 * @throws \Exception If the config for a module could not be loaded.
	 */
	public function loadConfig($moduleName, $modulePath)
	{
		if (!is_string($moduleName)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects first argument to be a string; received "%s"',
	            __METHOD__,
	            (is_object($moduleName) ? get_class($moduleName) : gettype($moduleName))
	        ));
	    }

	    if (!is_string($modulePath)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects second argument to be a string; received "%s"',
	            __METHOD__,
	            (is_object($modulePath) ? get_class($modulePath) : gettype($modulePath))
	        ));
	    }

	    $filename = sprintf('%s.xml', $moduleName);
		$filename = Strings::addTrailing($modulePath, '/') . $filename;

		if (!file_exists($filename)) {
			throw new \Exception(sprintf('Config for module %s at path "%s" does not exist', $moduleName, $filename));
		}

        return simplexml_load_file($filename);
	}
}