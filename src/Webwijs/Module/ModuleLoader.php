<?php

namespace Webwijs\Module;

use Webwijs\File\FileFinder;

/**
 * Module Loaders
 *
 * Loads the modules by iterating through the given directories
 *
 * @author Leo Flapper <leo@webwijs.nu>
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.1.0
 * @since 1.1.0
 */ 
class ModuleLoader implements ModuleLoaderInterface
{
	/**
	 * Searches for modules by their .xml config and returns an array which will be used to load the modules.
	 *
	 * @param FileFinder $paths The files that were found.
	 * @return array $modules associative array containing modules
	 */
	public function load(FileFinder $paths)
	{
	    $modules = [];

	    /** @var \SplFileInfo $fileInfo */
        foreach ($paths as $fileInfo) {
			$moduleName = $fileInfo->getBaseName();
            $modules[$moduleName] = array(
                'name' => $moduleName,
                'path' => $fileInfo->getPathname()
            );
		}
		
		return $modules;
	}
}