<?php

namespace Webwijs\Module\Finder;

use Webwijs\File\FilterInterface;
use Webwijs\Util\Strings;

/**
 * The ModuleFilter retains directories that represent a Webwijs module.
 *
 * @author Leo Flapper <leo@webwijs.nu>
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.1.0
 * @since 1.1.0
 */
class ModuleFilter implements FilterInterface
{
	/**
	 * Returns true if the the file represents a module directory.
	 *
	 * @param \SplFileInfo $fileInfo the file that will be tested.
	 * @return boolean true if the file represents a module directory.
	 */
	public function accept(\SplFileInfo $file)
	{
		$isModuleDir = false;
		
		if ($file->isDir()) {
			$filename = sprintf('%s.xml', ucfirst($file->getBasename()));
			$filename = Strings::addLeading($file->getPathname(), DIRECTORY_SEPARATOR) . $filename;
			
			$isModuleDir = is_file($filename);
		}
		
		return $isModuleDir;
	}
}