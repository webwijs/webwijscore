<?php

namespace Webwijs\Module\Finder;

use Webwijs\File\FilterInterface;

/**
 * The TemplateFilter retains files that represent Webwijs templates.
 */
class TemplateFilter implements FilterInterface
{

    /**
     * Returns true if the specified file is acceptable.
     *
     * @param \SplFileInfo $file the file to be tested.
     * @return bool true if the specified file is accepted, false otherwise.
     */
    public function accept(\SplFileInfo $file)
    {
        if ($file->isFile() && $file->getExtension() === 'php') {
            return $this->isTemplateFile($file->getFilename());
        }

        return false;
    }

    /**
     * Returns true if the specified filename represents a template file.
     *
     * @param string $filename The filename that will be tested.
     * @return bool True if the specified file is a template file.
     */
    private function isTemplateFile($filename)
    {
        return (strpos($filename, 'template-') === 0);
    }

}