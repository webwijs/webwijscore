<?php

namespace Webwijs\Module;

use Doctrine\Common\Cache\CacheProvider;
use Webwijs\AbstractBootstrap;
use Webwijs\Container\ActionManagerInterface;
use Webwijs\Container\ContainerInterface;
use Webwijs\File\FileFinder;
use Webwijs\Module\Config\Loader as ConfigLoader;
use Webwijs\Module\Finder\ModuleFilter;
use Webwijs\Util\Strings;

/**
 * Module Manager
 *
 * Handling the modules with it's data
 *
 * @author Leo Flapper <leo@webwijs.nu>
 * @author Chris Harris <chris@webwijs.nu>
 * @version 1.1.0
 * @since 1.1.0
 */
class ModuleManager
{
	/**
	 * The cache key to use for caching the routes
	 */
	const CACHE_KEY = 'modules';

	/**
	 * The module container
	 * @var array $modules, array containing modules
	 */
	protected static $modules = array();

	/**
	 * Contains the options for the module manager
	 * @var ModuleOptions $moduleOptions contains the module manager options
	 */
	private $moduleOptions;
	
	/**
	 * The absolute path to the template directory.
	 *
	 * @var string $templateDir
	 */
	private $templateDir;
	
	/**
	 * The URI to the template directory.
	 *
	 * @var string $templateUri
	 */
	private $templateUri;
    
    /**
     * @var ContainerInterface|null
     */
	private $container;
    
    /**
     * @var ActionManagerInterface|null
     */
	private $actionManager;
    
    /**
     * Sets the module manager options
     *
     * @param ModuleOptions $moduleOptions contains the module manager options
     * @param ContainerInterface|null $container
     * @param ActionManagerInterface|null $actionManager
     */
	public function __construct(ModuleOptions $moduleOptions, ContainerInterface $container = null, ActionManagerInterface $actionManager = null )
	{
		$this->setModuleOptions($moduleOptions);
		$this->container = $container;
		$this->actionManager = $actionManager;
	}

	/**
	 * Loads the modules from the given path.
	 * 
	 * @param  string $path the path to load the modules from
	 * @return array
	 */
	public function loadModules($path)
	{
		$fileFinder = new FileFinder(
		    \RecursiveDirectoryIterator::SKIP_DOTS,
            \RecursiveIteratorIterator::SELF_FIRST
        );
        $fileFinder->addPath($path);
        $fileFinder->setMaxDepth(0);
        $fileFinder->setFilter(new ModuleFilter());

		$loader = new CachedModuleLoader(new ModuleLoader(), $this->getCacheProvider(), self::CACHE_KEY);

		if ($modules = $loader->load($fileFinder)){
			$configLoader = new ConfigLoader();

			foreach ($modules as $module){
				if (isset($module['name'], $module['path'])) {
					$config = $configLoader->loadConfig($module['name'], $module['path']);
					$moduleInfo = new ModuleInfo($module['name'], $module['path'], $this->getModuleUri($module['path']), $config);
	    			$this->setModule($moduleInfo->getName(), $moduleInfo);
	    			
	    			do_action('module_loaded', $moduleInfo, $this->loadBootstrap($moduleInfo));
				}
			}
		}

		return $this->getModules();
	}

	/**
	 * Loads the module bootstrap
	 *
	 * @param ModuleInfo $moduleInfo the module info
	 * @return null|AbstractBootstrap The bootstrap for the loaded module.
	 * @throws \Exception If no bootstrap was found for the specified module.
	 */
 	public function loadBootstrap(ModuleInfo $moduleInfo)
    {
    	$bootstrap = null;
    	
        if ($moduleInfo->isActive() && $moduleInfo->getPath() && $moduleInfo->getName()) {
            $file = $moduleInfo->getPath().'/Bootstrap.php';
            if (!file_exists($file)) {
	            throw new \Exception(sprintf(
                    'Bootstrap not found for module %s at path %s',
		            $moduleInfo->getName(),
		            $moduleInfo->getPath()
	            ));
            }
	
	        include_once($file);
            
	        $className = $moduleInfo->getNamespace().'\Bootstrap';
	        $bootstrap = new $className($this->container, $this->actionManager);
        }
        
        return $bootstrap;
    }

	/**
	 * Sets a single module.
	 * The name will be used as key for the module.
	 *
	 * @param string $name the name of the module which will be used as key
	 * @param ModuleInfo $module a single module containing it's data.
	 */
	private function setModule($name, ModuleInfo $module)
	{
		if (!is_string($name)) {
	        throw new \InvalidArgumentException(sprintf(
	            '%s: expects a string argument; received "%s"',
	            __METHOD__,
	            (is_object($name) ? get_class($name) : gettype($name))
	        ));
	    }

    	$lookup = strtolower($name);
    	   	
    	static::$modules[$lookup] = $module;
	}

	/**
	 * Returns the module uri by the directory given.
	 * 
	 * @param  string $path the directory to generate the uri from
	 * @return string the generated module directory uri
	 */
	private function getModuleUri($path)
	{
		$directory = $this->getTemplateUri();
	    $themeDir = $this->getTemplateDir();
	    
	    if (Strings::startsWith($path, $themeDir)) {
		    $path = substr($path, strlen($themeDir));
	    }
		
		return $directory . Strings::addLeading($path, DIRECTORY_SEPARATOR);
	}
	
	/**
	 * Returns the absolute path to the template directory.
	 *
	 * @return string The absolute path to the template directory.
	 */
	private function getTemplateDir()
	{
		if ($this->templateDir === null) {
			$this->templateDir = get_template_directory();
		}
		
		return $this->templateDir;
	}
	
	/**
	 * Returns the URI to the template directory.
	 *
	 * @return string The URI to the template directory.
	 */
	private function getTemplateUri()
	{
		if ($this->templateUri === null) {
			$this->templateUri = get_template_directory_uri();
		}
		
		return $this->templateUri;
	}
	
	/**
	 * Returns the cache provider
	 * @return CacheProvider returns the cache provider
	 */
	public function getCacheProvider()
	{
		return $this->moduleOptions->getCacheProvider();
	}

	/**
	 * Returns a collection of loaded modules.
	 * 
	 * @return array a collection of modules, or empty array if no modules have been loaded.
	 */
	public static function getModules()
	{
		return static::$modules;
	}

	/**
	 * Sets the module manager options class
	 * @param ModuleOptions $moduleOptions the module manager options class
	 */
	private function setModuleOptions(ModuleOptions $moduleOptions)
	{
		$this->moduleOptions = $moduleOptions;
	}
	
}
