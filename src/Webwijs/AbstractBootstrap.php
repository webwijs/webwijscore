<?php

namespace Webwijs;

use Webwijs\Container\ActionManagerInterface;
use Webwijs\Container\ContainerInterface;

abstract class AbstractBootstrap
{
    /**
     * @var ContainerInterface|null
     */
    private $container;
    
    /**
     * @var ActionManagerInterface|null
     */
    private $actionManager;
    
    public function __construct(ContainerInterface $container = null, ActionManagerInterface $actionManager = null )
    {
        $this->container = $container;
        $this->actionManager = $actionManager;
    }
    
    protected function getContainer(): ?ContainerInterface
    {
        return $this->container;
    }
    
    protected function getActionManager(): ?ActionManagerInterface
    {
        return $this->actionManager;
    }
    
    public function init()
    {
        foreach (get_class_methods($this) as $method) {
            if (strpos($method, '_init') === 0) {
                $this->$method();
            }
        }
    }
}
