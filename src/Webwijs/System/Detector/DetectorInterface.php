<?php

namespace Webwijs\System\Detector;

use Webwijs\Http\Request;

/**
 * A detector operates on the HTTP request object and determines what device the client used to
 * visit the site.
 *
 * @author Chris Harris
 * @version 0.0.9
 */
interface DetectorInterface
{
    /**
     * Detect device the client is using.
     *
     * @param Request $request a HTTP request object.
     * @return int a number representing a specific device, or -1 if the device could not be determined.
     */
    public function detect(Request $request);

    /**
     * Set the next detector that will handle the request if a device type could not be determined.
     *
     * @param DetectorInterface $detector a detector that is called when the device type could not be determined.
     */
    public function setNextDetector(DetectorInterface $detector);
}
