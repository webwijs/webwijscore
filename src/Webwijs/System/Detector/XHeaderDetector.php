<?php

namespace Webwijs\System\Detector;

use Webwijs\System\Device;

use Webwijs\Http\Request;

/**
 * A detector that determines the device based on possible X headers.
 *
 * @author Chris Harris
 * @version 0.0.9
 */
class XHeaderDetector extends AbstractDetector
{
    /**
     * {@inheritDoc}
     */
    public function detect(Request $request)
    {
        $retval = -1;
        if ($request->getServer('HTTP_X_WAP_PROFILE') !== null || $request->getServer('HTTP_PROFILE') !== null) {
            $retval = Device::PHONE;
        } else if ($this->detector !== null) {
            $retval = $this->detector->detect($request);
        }

        return $retval;
    }
}
