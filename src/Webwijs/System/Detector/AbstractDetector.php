<?php

namespace Webwijs\System\Detector;

/**
 * This class provides a skeletal implementation of the DetectorInterface interface.
 *
 * @author Chris Harris
 * @version 0.0.9
 */
abstract class AbstractDetector implements DetectorInterface
{
    /**
     * A device detector.
     *
     * @var DetectorInterface
     */
    protected $detector;

    /**
     * {@inheritDoc}
     */
    public function setNextDetector(DetectorInterface $detector)
    {
        $this->detector = $detector;
    }
}
